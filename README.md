Example project demonstrating the [debug tracing][trace] feature of GitLab Runner.

[trace]: https://docs.gitlab.com/ce/ci/variables/#debug-tracing